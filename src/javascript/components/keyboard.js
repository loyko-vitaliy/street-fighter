class Keyboard {
  constructor() {
    this.keys = {};

    document.addEventListener('keydown', ({ code }) => this.update(code, true));
    document.addEventListener('keyup', ({ code }) => this.update(code, false));
  }

  update(code, pressed) {
    this.keys[code] = pressed;
  }
}

export const keyboard = new Keyboard();
