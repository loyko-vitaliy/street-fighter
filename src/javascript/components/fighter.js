class Fighter {
  constructor({ fighterInfo: { name, health, attack, defense }, healthBarSelector }) {
    this.name = name;
    this.maxHealth = health;
    this.currentHealth = health;
    this.attack = attack;
    this.defense = defense;
    this.critAttack = {
      delay: 10,
      critChance: 2,
      isAvailable: true,
    };
    this.healthBar = document.querySelector(healthBarSelector);
  }

  updateHealthBar(damage) {
    const currentHealth = this.currentHealth - damage;
    this.currentHealth = currentHealth > 0 ? currentHealth : 0;

    const healthBarWidth = (this.currentHealth / this.maxHealth) * 100;
    this.healthBar.style.width = `${healthBarWidth}%`;
  }

  setCritAttackDelay() {
    this.critAttack.isAvailable = false;

    setTimeout(() => {
      this.critAttack.isAvailable = true;
    }, this.critAttack.delay * 1000);
  }
}

export default Fighter;
