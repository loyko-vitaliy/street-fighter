import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    const fighterImage = createCustomFighterImage(fighter);
    const fighterInfo = createFighterInfo(fighter);

    fighterElement.append(fighterImage, fighterInfo);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createCustomFighterImage(fighter) {
  const imageContainer = createElement({ tagName: 'div', className: 'fighter-preview__img-container' });
  const fighterImage = createFighterImage(fighter);
  imageContainer.append(fighterImage);

  return imageContainer;
}

function createFighterInfo(fighter) {
  const fighterAttributes = ['name', 'health', 'defense', 'attack'];

  const infoContainer = createElement({ tagName: 'div', className: 'fighter-preview__info' });
  for (const fighterAttribute of fighterAttributes) {
    const attribute = createAttribute({ label: fighterAttribute, value: fighter[fighterAttribute] });
    infoContainer.append(attribute);
  }

  return infoContainer;
}

function createAttribute({ label, value }) {
  const attribute = createElement({ tagName: 'div', className: 'fighter-preview__attribute' });

  const attributeLabel = createElement({ tagName: 'div', className: 'fighter-preview__attribute-label' });
  attributeLabel.textContent = label;

  const attributeValue = createElement({ tagName: 'span', className: 'fighter-preview__attribute-value' });
  attributeValue.textContent = value;

  attribute.append(attributeLabel, attributeValue);

  return attribute;
}
