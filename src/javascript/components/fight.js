import { controls } from '../../constants/controls';
import { getRandomNumber } from '../helpers/getRandomNumber';
import { keyboard } from './keyboard';
import Fighter from './fighter';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const playerOne = new Fighter({ fighterInfo: firstFighter, healthBarSelector: '#left-fighter-indicator' });
    const playerTwo = new Fighter({ fighterInfo: secondFighter, healthBarSelector: '#right-fighter-indicator' });

    document.addEventListener('keydown', ({ code }) => {
      if (playerOne.currentHealth > 0 && playerTwo.currentHealth > 0) {
        if (
          (keyboard.keys[controls.PlayerOneAttack] && keyboard.keys[controls.PlayerTwoBlock]) ||
          (keyboard.keys[controls.PlayerTwoAttack] && keyboard.keys[controls.PlayerOneBlock])
        ) {
          return;
        } else if (
          keyboard.keys[controls.PlayerOneAttack] &&
          !keyboard.keys[controls.PlayerOneBlock] &&
          controls.PlayerOneAttack === code
        ) {
          playerTwo.updateHealthBar(getDamage(firstFighter, secondFighter));
        } else if (
          keyboard.keys[controls.PlayerTwoAttack] &&
          !keyboard.keys[controls.PlayerTwoBlock] &&
          controls.PlayerTwoAttack === code
        ) {
          playerOne.updateHealthBar(getDamage(secondFighter, firstFighter));
        } else if (
          !keyboard.keys[controls.PlayerOneBlock] &&
          controls.PlayerOneCriticalHitCombination.every((key) => keyboard.keys[key]) &&
          playerOne.critAttack.isAvailable &&
          controls.PlayerOneCriticalHitCombination.includes(code)
        ) {
          playerTwo.updateHealthBar(getHitPower(firstFighter, playerOne.critAttack.critChance));
          playerOne.setCritAttackDelay();
        } else if (
          !keyboard.keys[controls.PlayerTwoBlock] &&
          controls.PlayerTwoCriticalHitCombination.every((key) => keyboard.keys[key]) &&
          playerTwo.critAttack.isAvailable &&
          controls.PlayerTwoCriticalHitCombination.includes(code)
        ) {
          playerOne.updateHealthBar(getHitPower(secondFighter, playerTwo.critAttack.critChance));
          playerTwo.setCritAttackDelay();
        }
      }

      if (playerTwo.currentHealth === 0) {
        resolve(firstFighter);
      } else if (playerOne.currentHealth === 0) {
        resolve(secondFighter);
      }
    });
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);

  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter, critChance = getRandomNumber(1, 2)) {
  return fighter.attack * critChance;
}

export function getBlockPower(fighter, dodgeChance = getRandomNumber(1, 2)) {
  return fighter.defense * dodgeChance;
}
